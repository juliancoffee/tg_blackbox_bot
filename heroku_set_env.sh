. env.sh

heroku config:set HOST="$HOST"
heroku config:set TELEGRAM_TOKEN="$TELEGRAM_TOKEN"
heroku config:set TG_ADMINS_GROUP_ID="$TG_ADMINS_GROUP_ID"
heroku config:set SUCCESS_TEXT="$SUCCESS_TEXT"
heroku config:set START_TEXT="$START_TEXT"
heroku config:set SUCCESS_ANSWER_TEXT="$SUCCESS_ANSWER_TEXT"
heroku config:set STORAGE_FILE="$STORAGE_FILE"
