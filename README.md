based on [https://gitlab.com/modos189/tg_blackbox_bot]

# Telegram blackbox bot

Telegram bot that allows you to organize anonymous feedback for your project.
User messages sent to the bot will be copied to administrators chat.
Administrators can reply to message.

Implemented via webhooks, so you'll need to host it somewhere.

## Quickstart
To run you should export environment variables:

 - `HOST` - host of your bot to listen for webhooks.
 - `PORT` - port.
 - `TELEGRAM_TOKEN` - telegram bot token
 - `TG_ADMINS_GROUP_ID` - telegram group id with admins (example: -100xxxx...)
 - `START_TEXT` - Message on receipt of the /start command
 - `SUCCESS_TEXT` - The text of the response to the user's message
 - `SUCCESS_ANSWER_TEXT` - Text of the message to the administrator about the successful response to the user's message
 - `STORAGE_FILE` - (optional) Path to sqlite file. If not specified, database in memory is used
 - `RUST_LOG` - (optional) Set to "debug" to see debug messages in console

### Option 1: Build crate

1. Install [Rust].
2. Setup your bot with [@botfather](https://t.me/botfather).
3. Build crate:
```console
cargo build
```
4. Start the bot:
```console
$ export HOST=insert-the-host-of-your-bot-here.com
$ export PORT=8000
$ export TELEGRAM_TOKEN=123456789:blablabla
$ export TG_ADMINS_GROUP_ID=-1001234567890
$ export START_TEXT=Send any information anonymously
$ export SUCCESS_TEXT=Your message is accepted
$ export SUCCESS_ANSWER_TEXT=Reply sent
$ export STORAGE_FILE=storage.db
$ export RUST_LOG=debug
$ cargo run
```
5. Send `/start` to your telegram bot.

### Option 2: Heroku CLI
1. Register on [Heroku].
2. Setup your bot with [@botfather](https://t.me/botfather).
3. Login using `heroku login`. 
4. Create an app with rust buildpack.
```console
heroku create --buildpack https://github.com/emk/heroku-buildpack-rust.git
```
5. Create env.sh file with all environment variables as above.
PORT is not needed, Heroku will set one for you.
```bash
# heroku host
HOST=quotes-admin-bot.herokuapp.com

# set keys
TELEGRAM_TOKEN=1111111:blalablah
TG_ADMINS_GROUP_ID=-123456789

# localisation
START_TEXT=Send any information anonymously
SUCCESS_TEXT=Your message is accepted
SUCCESS_ANSWER_TEXT=Reply sent

# utils
STORAGE_FILE=storage.db

# exports
export HOST
export TELEGRAM_TOKEN
export TG_ADMINS_GROUP_ID
export START_TEXT
export SUCCESS_TEXT
export SUCCESS_ANSWER_TEXT
export STORAGE_FILE
```
6. Run `heroku_set_env.sh` if you have bash, it will push all variables to Heroku.
Otherwise, just set all variables yourself using `heroku config:set`.
7. Deploy with `git push heroku master`.

[rust]: https://doc.rust-lang.org/cargo/getting-started/installation.html
[heroku]: https://devcenter.heroku.com/categories/reference
