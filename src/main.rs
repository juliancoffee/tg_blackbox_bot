extern crate pretty_env_logger;
#[macro_use]
extern crate log;

use std::{env, process};
use tokio_rusqlite::Connection;

use teloxide::{
    dispatching::update_listeners::webhooks, prelude::*, types::ChatId,
    utils::command::BotCommands,
};
use url::Url;

mod anonymizer;
mod database;

#[derive(Clone)]
struct ConfigParameters {
    conn: Connection,
    admins_group_id: ChatId,
    start_text: String,
    success_text: String,
    success_answer_text: String,
}

#[derive(BotCommands, Clone)]
#[command(rename = "lowercase", description = "These commands are supported:")]
enum Command {
    #[command(description = "Start bot")]
    Start,
}

fn env_not_set(name: &str) -> String {
    format!("Environment variable {} is not set", name)
}

fn set_token(token: &str) {
    env::set_var("TELOXIDE_TOKEN", token);
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    info!("Starting bot...");

    let admins_group_id: i64 = env::var("TG_ADMINS_GROUP_ID")
        .expect(&env_not_set("TG_ADMINS_GROUP_ID"))
        .parse::<i64>()
        .expect("TG_ADMINS_GROUP_ID must be a number");

    if admins_group_id >= 0 {
        error!("TG_ADMINS_GROUP_ID is incorrect, must be a negative number");
        process::exit(1)
    }

    let conn = match env::var("STORAGE_FILE") {
        Ok(path) => Connection::open(path)
            .await
            .expect("Database connection error"),
        Err(_) => Connection::open_in_memory()
            .await
            .expect("Database connection error"),
    };

    let parameters = ConfigParameters {
        conn: conn.clone(),
        admins_group_id: ChatId(admins_group_id),
        start_text: env::var("START_TEXT").expect(&env_not_set("START_TEXT")),
        success_text: env::var("SUCCESS_TEXT")
            .expect(&env_not_set("SUCCESS_TEXT")),
        success_answer_text: env::var("SUCCESS_ANSWER_TEXT")
            .expect(&env_not_set("SUCCESS_ANSWER_TEXT")),
    };

    database::init(conn)
        .await
        .expect("Database initialization error");

    // Set token from an environment variable
    let token =
        env::var("TELEGRAM_TOKEN").expect(&env_not_set("TELEGRAM_TOKEN"));
    set_token(&token);
    let bot = Bot::from_env().auto_send();

    // Handlers
    let command_handler = teloxide::filter_command::<Command, _>()
        .branch(dptree::case![Command::Start])
        .endpoint(anonymizer::start);

    let handler = Update::filter_message()
        .branch(command_handler)
        .branch(
            dptree::filter(|msg: Message, cfg: ConfigParameters| {
                msg.chat.id == cfg.admins_group_id
            })
            .endpoint(anonymizer::reply),
        )
        .branch(
            dptree::filter(|msg: Message| msg.chat.is_private())
                .endpoint(anonymizer::forward),
        );

    // Webhooks listener
    // (from teloxide examples)
    let token = bot.inner().token();

    // Heroku auto defines a port value
    let port: u16 = env::var("PORT")
        .expect("PORT env variable is not set")
        .parse()
        .expect("PORT env variable value is not an integer");

    let addr = ([0, 0, 0, 0], port).into();

    // Heroku host example: "heroku-ping-pong-bot.herokuapp.com"
    let host = env::var("HOST").expect("HOST env variable is not set");
    let url = format!("https://{host}/webhooks/{token}");
    let url = Url::parse(&url).unwrap();

    let listener =
        webhooks::axum(bot.clone(), webhooks::Options::new(addr, url))
            .await
            .expect("Couldn't setup webhook");

    // Run
    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![parameters])
        .default_handler(|upd| async move {
            warn!("Unhandled update: {:?}", upd);
        })
        .error_handler(LoggingErrorHandler::with_custom_text(
            "An error has occurred in the dispatcher",
        ))
        .enable_ctrlc_handler()
        .build()
        .dispatch_with_listener(
            listener,
            LoggingErrorHandler::with_custom_text(
                "An error has occured in webhooks listener",
            ),
        )
        .await;
}
